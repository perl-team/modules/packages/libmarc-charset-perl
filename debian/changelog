libmarc-charset-perl (1.35-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Remove constraints unnecessary since buster
    * Build-Depends: Drop versioned constraint on perl.
    * libmarc-charset-perl: Drop versioned constraint on perl in Depends.

  [ gregor herrmann ]
  * Use variables in debian/rules.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Drop unneeded unversioned 'perl' dependency.
  * Enable all hardening flags.
  * Annotate test-only build dependencies with <!nocheck>.
  * Drop obsolete lintian override.

 -- gregor herrmann <gregoa@debian.org>  Tue, 02 Aug 2022 19:01:18 +0200

libmarc-charset-perl (1.35-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ intrigeri ]
  * Declare that debian/rules does not need root.
  * Declare compliance with Policy 4.1.5.

  [ gregor herrmann ]
  * Add empty debian/tests/pkg-perl/syntax-skip to activate more syntax.t
    tests.

  [ Niko Tyni ]
  * Depend and Build-Depend on Perl versions that guarantee database
    binary compatibility with libgdbm6. (Closes: #923238)

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Mar 2019 01:09:35 +0100

libmarc-charset-perl (1.35-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Store the generated character mapping database in a reproducible way.
  * Declare the package autopkgtestable.
  * Update to Standards-Version 3.9.6. No changes.

 -- Niko Tyni <ntyni@debian.org>  Fri, 28 Aug 2015 11:59:21 +0300

libmarc-charset-perl (1.35-1) unstable; urgency=low

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: new Upstream-Contact.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.4.
  * Add lintian-override for pkg-perl specific debhelper test.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Oct 2013 20:54:01 +0200

libmarc-charset-perl (1.33-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- gregor herrmann <gregoa@debian.org>  Wed, 17 Aug 2011 19:31:30 +0200

libmarc-charset-perl (1.32-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Use minimal debian/rules with debhelper 8 and override_* targets.
  * Use source format 3.0 (quilt).
  * Make build-dep on perl unversioned.
  * debian/copyright: Formatting changes; refer to "Debian systems" instead of
    "Debian GNU/Linux systems"; refer to /usr/share/common-licenses/GPL-1.
  * Bump Standards-Version to 3.9.2.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 04 Jul 2011 12:15:42 +0200

libmarc-charset-perl (1.31-1) unstable; urgency=low

  * New upstream release
  * Added me to Uploaders and copyright file
  * Standards-Version: 3.9.1 (no changes necessary)

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Fri, 08 Oct 2010 11:57:08 +0200

libmarc-charset-perl (1.2-1) unstable; urgency=low

  * New upstream release
    + uses Storable::nfreeze instead of Storable::freeze to get a more
      portable character set database.
      Reported and fixed by Niko Tyni. Closes: #579517
  * Standards-Version: 3.8.4 (no changes necessary)

 -- Damyan Ivanov <dmn@debian.org>  Wed, 12 May 2010 14:21:04 +0300

libmarc-charset-perl (1.1-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * New upstream release
  * debian/patches/*:
    - Remove since gdbm.patch was applied upstream
  * debian/control:
    - Add myself to list of Uploaders
    - Drop quilt Build-Depends
    - Bump Standards-Version to 3.8.2 (No changes)
    - Bump debhelper Build-Depends to (>= 7)
  * debian/README.source:
    - Remove since quilt is no longer used
  * debian/rules:
    - Refresh to use newer format
  * debian/copyright:
    - Refresh to use newer format
    - Add myself to debian/* copyright
  * debian/compat:
    - Bump to 7
  * debian/watch:
    - Remove comment
  * debian/links:
    - Use to make symlink instead of debian/rules

  [ gregor herrmann ]
  * Add debian/libmarc-charset-perl.dirs and debian/clean, re-add a manual
    `mv' to debian/rules in order to get the created Table file installed into
    the right place and removed on clean (done by debian/rules before).
  * debian/copyright: add more contributors to the packaging copyright.
  * Improve short description, thanks to Jari Aalto for the bug report
    (closes: #550289).

 -- Nathan Handler <nhandler@ubuntu.com>  Thu, 02 Jul 2009 01:47:43 +0000

libmarc-charset-perl (1.0-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: add upstream source location.
  * debian/control: change my email address.
  * Set Standards-Version to 3.8.0 (no changes).
  * Refresh debian/rules, no functional changes.

  [ Damyan Ivanov ]
  * add gdbm.patch by Niko Tyni. Marc::Charset::Table now uses a GDMB database,
    reducing its size from 400M to 5M. Closes: #481505
  * add quilt to the build process
  * adapt debian/rules to the new filenames GDBM uses
  * add README.source pointing to quilt documentation

 -- Damyan Ivanov <dmn@debian.org>  Sun, 24 Aug 2008 08:36:47 +0300

libmarc-charset-perl (0.98-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/rules: Remove /usr/lib/perl5 only if it exists.

  [ Damyan Ivanov ]
  * debian/watch: use dist-based URL; better pattern
  * debian/rules:
    + use "$@" when touching stamps
    + move test suite from install to build target
    + remove stamps before realclean
    + drop unneeded argument-less dh_link call
    + drop unneeded dh_strip call
  * debhelper compatibility level 6
  * Standards-Version: 3.7.3 (no changes)

 -- Damyan Ivanov <dmn@debian.org>  Wed, 23 Jan 2008 15:29:49 +0200

libmarc-charset-perl (0.98-1) unstable; urgency=low

  * New upstream release
    Improvement of Arabic code tables

 -- Damyan Ivanov <dmn@debian.org>  Fri, 10 Aug 2007 14:19:47 +0300

libmarc-charset-perl (0.97-2) unstable; urgency=medium

  * Set Architecture to any, since the package contains architecture
    dependent SDBM files. (Closes: #429030)
  * Accordingly, install the SDBM files under /usr/lib instead of /usr/share.
  * Urgency set to medium because of an RC bug fix.

 -- Niko Tyni <ntyni@iki.fi>  Sun, 17 Jun 2007 21:35:01 +0300

libmarc-charset-perl (0.97-1) unstable; urgency=low

  * New upstream release
  * Fixed debian/watch to point to www.cpan.org
  * Added myself to uploaders
  * Wrapped long fields in debian/control

 -- Damyan Ivanov <dmn@debian.org>  Fri, 08 Jun 2007 01:34:04 +0300

libmarc-charset-perl (0.96-1) unstable; urgency=low

  * New upstream release.
  * Set Standards-Version to 3.7.2 (no changes).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 19 May 2007 00:53:27 +0200

libmarc-charset-perl (0.95-2) unstable; urgency=low

  * Fix typo in Description

 -- Frank Lichtenheld <djpig@debian.org>  Sat,  2 Dec 2006 00:18:25 +0100

libmarc-charset-perl (0.95-1) unstable; urgency=low

  * Initial Release (closes: #251873).
  * Partly used Georg Baum's diff from #251873, thanks!

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 24 Mar 2006 18:02:35 +0100
